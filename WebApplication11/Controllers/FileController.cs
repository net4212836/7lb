﻿using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Text;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class FileController : Controller
    {
        public IActionResult DownloadFile()
        {
            return View();
        }

        [HttpPost]
        public IActionResult DownloadFile(FileViewModel fileInfo)
        {
            byte[] mas = Encoding.UTF8.GetBytes($"{fileInfo.FirstName} {fileInfo.LastName}");
            return File(mas, "text/plain", $"{fileInfo.FileName}.txt");
        }

    }
}
